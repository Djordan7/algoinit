package fr.eql.ai110.algo.init.demo;

public class _1_HelloWorld {

	public static void main(String[] args) {
		
		// Commentaire monoligne avec "//"
		
		/*
		 * Commentaire multiligne avec "/*"
		 * suivi de la touche "entrée" pour
		 * chaque ligne supplémentaire
		 */
		
		// Impression simple en console
		System.out.print("Hello World !");
		// Impression avec saut de ligne
		System.out.println("How are you ?");
		
		/* 
		 * Saut de ligne :
		 * CR + LF = Carriage Return + Line Feed -> \r\n
		 * Windows : \r\n
		 * Linux : \n
		 * Mac : \r	
		 */
		 
		System.out.println("Goodbye\r\nWorld");
		System.out.println("Goodbye\rWorld");
		System.out.println("Goodbye\nWorld");
		
		// \t = tabulation
		System.out.println("\tHello again !");	
		}
}
