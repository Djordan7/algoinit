package fr.eql.ai110.algo.init.demo;

import java.util.Scanner;

public class _6_DemoSwitch {
	
	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		String weapon = "";
		System.out.println("Un troll vous attaque. "
				+ "Quelle arme souhaitez-vous utiliser ? (épée/arc/magie)");
		weapon = myScanner.nextLine();
		myScanner.close();
		
		// Structure switch permettant de faire des opérations différentes
		// en fonction des différentes valeurs d'une variable.
		// Equivaut à :
		// if (a = 0) {
		// } else if (a = 1) {
		// } else if (a = 2) {
		// } etc...
		switch (weapon) {
			case "épée" :
				System.out.println("Vous avez choisi la voie du guerrier. "
						+ "Votre coup d'épée fait perdre 2PV au troll.");
				// Le "break" sert à sortir du switch
				// une fois les actions du cas sont effectués.
				break;
			case "arc" :
				System.out.println("Vous avez choisi la voie du chasseur. "
						+ "Votre flèche fait perdre 1PV au troll.");
				break;
			case "magie" :
				System.out.println("Vous avez choisi la voie du magicien. "
						+ "Malheureusement, le troll est résistant à la magie."
						+ "Il vous mange.");
				break;
			default :
				System.out.println("Vous faites le malin. "
						+ "Vous êtes donc tout nu."
						+ "Le troll vous mange.");
		}
	}
}
