package fr.eql.ai110.algo.init.demo;

import java.util.Scanner;

public class _7_DemoLoops {

	public static void main(String[] args) {
		
		System.out.println("*** Boucle FOR ***");
		// int i = 1 : initialisation d'un indice (borne inférieure)
		// i < 10 : Borne supérieure
		// i++ : l'incrémentation de l'indice à chaque boucle
		for (int i = 1; i < 10; i++) {
			System.out.println(i + " tour(s) dans la boucle.");
		}
		
		// Traitement particulier pour la boucle dont l'indice est 1
		for (int i = 1; i < 10; i++) {
			String word = "";
			if (i == 1) {
				word = " tour ";
			} else {
				word = " tours ";
			}
			System.out.println(i + word + "dans la boucle.");
		}
		
		System.out.println("\r\n*** Différents moments d'incrémentation ***");
		int i;
		i = 0;
		System.out.println("i : " + i);
		System.out.println("i++ : " + i++);
		System.out.println("i : " + i);
		i = 0;
		System.out.println("i : " + i);
		System.out.println("++i : " + ++i);
		System.out.println("i : " + i);
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("\r\n*** Boucle WHILE ***");
		System.out.println("Tapez un nombre positif :");
		int number = myScanner.nextInt();
		
		while (number <= 0) {
			System.out.println("Erreur ! Votre nombre n'est pas"
					+ " positif. Veuillez en taper un autre :");
			number = myScanner.nextInt();
		}
		System.out.println("Merci ! Votre nombre est bien positif.");
		
		System.out.println("\r\n*** Boucle DO... WHILE ***");
		float credit = 1000f;
		float payment;
	
		do {
			System.out.println("Votre crédit est de " 
		+ credit + "€.\r\nQuelle somme souhaitez-vous rembourser ?");
			payment = myScanner.nextFloat();
			credit -= payment;
		} while (credit > 0);
		System.out.println("Merci ! Votre crédit est remboursé.");
		
		myScanner.close();
	}
}
