package fr.eql.ai110.algo.init.demo;

public class _4_DemoIfElse {

	public static void main(String[] args) {
		
		int x = 1;
		
		System.out.println("*** Condition avec opérateur OU (||)");
		if ((x == 1) || (x >= 1000 )) {
			System.out.println("Au moins une des deux conditions est remplie.");
		} else {
			System.out.println("Aucune des deux conditions n'est remplie.");
		}
		
		System.out.println("*** Condition avec opérateur OU EXCLUSIF (^)");
		if ((x == 1) ^ (x <= 1000 )) {
			System.out.println("Une seule des deux conditions est remplie.");
		} else {
			System.out.println("Les deux conditions sont remplies, "
					+ "ou aucune des deux.");
		}
		
		System.out.println("*** Condition avec opérateur ET (&&)");
		if ((x == 1) && (x >= 1000 )) {
			System.out.println("Les deux conditions sont remplies.");
		} else if (x <= 2) {
			System.out.println("Une condition alternative est remplie.");
		} else {
			System.out.println("Ni la première série de conditions, ni la"
					+ " deuxième, ne sont remplies.");
		}
		
		System.out.println("\r\n*** Négation (Lois de Morgan) ***");
		boolean isTrue, isAlsoTrue;
		
		System.out.println("\r\n--- Négation avec OU ---");
		/*
		 * La négation de la disjonction de deux propositions est équivalente
		 * à la conjonction des négations des deux propositions, ce qui
		 * signifie que :
		 * non(A ou B) est identique à (non A) et (non B).
		 */
		isTrue = !((x == 0) || (x != 1));
		// est équivalent à :
		isAlsoTrue = !(x == 0) && !(x != 1);
		// est également équivalent à :
		isAlsoTrue = (x != 0) && (x == 1);
		if (isTrue && isAlsoTrue) {
			System.out.println("Les deux conditions équivalentes sont remplies.");
		} else {
			System.out.println("Aucune des deux conditions ne sont remplies, "
					+ "ou elles ne sont pas équivalentes.");
		}
		
		System.out.println("\r\n--- Négation avec ET ---");
		/*
		 * La négation de la conjonction de deux propositions est équivalente à
		 * la disjonction des négations des deux propositions, ce qui signifie 
		 * que :
		 * non(A et B) est identique à (non A) ou (non B).
		 */
		isTrue = !((x == 1) && (x >= 1000));
		// est équivalent à :
		isAlsoTrue = !(x == 1) || !(x >= 1000);
		// est également équivalent à :
		isAlsoTrue = (x != 1) || (x < 1000);
		if (isTrue && isAlsoTrue) {
			System.out.println("Les deux conditions équivalentes sont remplies.");
		} else {
			System.out.println("Aucune des deux conditions ne sont remplies, "
					+ "ou elles ne sont pas équivalentes.");
		}
		
		System.out.println("\n\r*** Opérateurs logiques et bit à bit");
		String error = null;
		// error.isEmpty() renvoie une erreur
		// Opérateur logique OU : si la première condition est remplie,
		// la deuxième n'est pas testée.
		if ((x >= 0) || (error.isEmpty())) {
			System.out.println("OU : la deuxième condition n'est pas testée.");
		}
		// Opérateur bit à bit : les deux conditions sont comparées entre elles.
//		System.out.println("Une erreur va être générée ici.");
//		if ((x >= 0) | (error.isEmpty())) {
//			
//		}
		
		// Opérateur logique ET : si la première condition n'est pas remplie,
		// la deuxième n'est pas testée.
		System.out.println("ET : la deuxième condition n'est pas testée.");
		if ((x != 1) && (error.isEmpty())) {
			
		}
		// Opérateur bit à bit : les deux conditions sont comparées entre elles.
//		System.out.println("Une erreur va être générée ici.");
//		if ((x != 1) & (error.isEmpty())) {
//			
//		}
	}
}
