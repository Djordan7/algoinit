package fr.eql.ai110.algo.init.demo;

import java.util.Scanner;

public class _5_DemoInput {

	public static void main(String[] args) {
		
		// On crée le scanner qui récupérera l'entrée utilisateur en console
		Scanner monScanner = new Scanner(System.in);
		
		System.out.println("Saisissez votre nom :");
		// On affecte l'entrée utilisateur à la variable "nom" lorsque 
		// l'utilisateur appuie sur la touche "Enter"
		String nom = monScanner.nextLine();
		
		System.out.println("Saisissez votre âge :");
		int age = monScanner.nextInt();
		
		System.out.println("Bonjour " + nom + ".\r\nVous avez " + age + " ans.");
		
		// On met fin au scanner
		monScanner.close();
	}
}
