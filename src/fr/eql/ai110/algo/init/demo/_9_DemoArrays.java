package fr.eql.ai110.algo.init.demo;

public class _9_DemoArrays {
	
	public static void main(String[] args) {
		
		// Initialisation et peuplement d'un tableau de Strings
		// à une dimension
		String[] seasons = {"Printemps", "Eté", "Automne", "Hiver"};
		System.out.println("*** Lecture du 3ème élément (index 2)"
				+ " du tableau à une dimension");
		System.out.println(seasons[2]);
		
		System.out.println("\r\n*** Lecture de tous les éléments du "
				+ "tableau à une dimension");
		for (int i = 0; i < seasons.length; i++) {
			System.out.println(seasons[i]);
		}
		
		// Initialisation d'un tableau d'entiers à deux dimensions
		int[][] matrix = {
				{1,2,3},
				{4,5,6},
				{7,8,9}
		};
		
		System.out.println("\r\n*** Lecture de tous les éléments du "
				+ "tableau à deux dimensions");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
	}
}
