package fr.eql.ai110.algo.init.demo;

public class _3_DemoArithmetic {

	public static void main(String[] args) {
		
		// Déclaration de plusieurs variables du même type
		// sur une seule ligne
		int number1, number2, result1;
		float number3, number4, result2;
		
		number1 = 7;
		number2 = 2;
		
		System.out.println("*** Addition ***");
		result1 = number1 + number2;
		System.out.println(number1 + " + " + number2 + " = " + result1);
		result1++; // incrémente le résulat de 1.
		System.out.println("et plus 1 : " + result1);
		result1 += number2; // incrémente le résultat de la valeur de number2
		System.out.println("et plus " + number2 + " : " + result1);
		
		System.out.println("\r\n*** Soustraction ***");
		result1 = number1 - number2;
		System.out.println(number1 + " - " + number2 + " = " + result1);
		result1--; // décrémente le résulat de 1.
		System.out.println("et moins 1 : " + result1);
		result1 -= number2; // décrémente le résultat de la valeur de number2
		System.out.println("et moins " + number2 + " : " + result1);
		
		System.out.println("\r\n*** Multiplication ***");
		result1 = number1 * number2;
		System.out.println(number1 + " * " + number2 + " = " + result1);
		result1 *= number2; // multiplie le résultat par la valeur de number2
		System.out.println("et multiplié par " + number2 + " : " + result1);
		
		System.out.println("\r\n*** Division ***");
		number3 = 7f;
		number4 = 2f;
		result2 = number3 / number4;
		System.out.println(number3 + " / " + number4 + " = " + result2);
		result2 /= number4; // divise le résultat par la valeur de number4
		System.out.println("et divisé par " + number4 + " : " + result2);
		
		/*
		 * Division euclidienne (entière)
		 * 7 / 2 = 3.5
		 * 7 / 2 = 3 + 0.5
		 * 7 = 2 * (3 + 0.5)
		 * 7 = 2 * 3 + 2 * 0.5
		 * 7 = 2 * 3 + 1
		 * 
		 * 7 : dividende
		 * 2 : diviseur
		 * 3 : quotient
		 * 1 : reste
		 */
		
		System.out.println("\r\n*** Quotient d'une division entière ***");
		result1 = number1 / number2;
		System.out.println(number1 + " / " + number2 + " = " + result1);
		result1 /= number2;
		System.out.println("et divisé par " + number2 + " : " + result1);
		
		System.out.println("\r\n*** Reste de division entière (modulo)");
		result1 = number1 % number2;
		System.out.println(number1 + " modulo " + number2 + " = " + result1);
		result1 %= number2;
		System.out.println("et modulo " + number2 + " : " + result1);
	}
}
