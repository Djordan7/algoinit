package fr.eql.ai110.algo.init.demo;

public class _2_DemoVariable {

	public static void main(String[] args) {
		
		System.out.println("*** Déclaration ***");
		// On déclare une variable en précisant le type
		// avant le nom de la variable (ici "int")
		int number1;
		// On affecte une valeur à une variable avec le
		// signe "="
		number1 = 0;
		System.out.println(number1);
		number1 = 42;
		System.out.println(number1);
		
		// On peut déclarer une variable et lui affecter
		// une valeur sur la même ligne
		int number2 = 257;
		System.out.println(number2);

		System.out.println("\r\n*** Les nombres ***");
		/*
		 * -byte- : entier court codé sur un octet (-128 à +127)
		 * -short-: entier court stocké sur 2 octets (-32768 à +32767)
		 * -int- : entier codé sur 4 octets (-2*10^9 à 2*10^9)
		 * -long-: entier codé sur 8 octets (-9*10^18 à 9*10^18)
		 * 			Un long doit être suffixé par un L
		 * -float-: un nombre à virgule flottante codé sur 4 octet
		 * 			Un float doit être suffixé par un f
		 * -double-:un nombre à virgule flottante codé sur 8 octets
		 * 			Un double doit être suffixé par un d
		 */
		
		byte temperature = 42;
		// La grandeur suivante est trop élevée pour un byte :
		//temperature = 1565162456486346528636;
		short speed = 32000;
		int distance = 420000000;
		distance = 0b111;
		long lightYear = 9460700000000000L;
		float pi = 3.141592653f;
		double third = 0.3333333333333333333333333333333333334d;
		
		System.out.println(temperature);
		System.out.println(speed);
		System.out.println(distance);
		System.out.println(lightYear);
		System.out.println(pi);
		System.out.println(third);
		
		System.out.println("\r\n*** Les booléens ***");
		// Un booléen (contient "true" ou "false" comme valeur)
		// est stocké 1 octet.
		boolean isReady = true;
		System.out.println(isReady);
		isReady = false;
		System.out.println(isReady);
		
		System.out.println("\r\n*** Les caractères ***");
		// -char- : s'écrit par convention entre ''. Stocké sur 2 octets.
		char letter = 'a';
		System.out.println(letter);
		
		System.out.println("\r\n*** Les chaînes de caractères ***");
		// 3 façons de déclarer une chaîne de caractères :
		// 1 :
		String sentence1 = "Some words";
		// 2 :
		String sentence2 = new String();
		sentence2 = "Other words";
		// 3 :
		String sentence3 = new String("Final words");
		
		System.out.println(sentence1);
		System.out.println(sentence2);
		System.out.println(sentence3);
		
		System.out.println("\r\n*** Concaténation des chaînes de caractères");
		String bigSentence = sentence1 + " " + sentence2 + " " + sentence3;
		System.out.println(bigSentence);
		
		System.out.println("\r\n*** Impression d'un caractère d'une chaîne");
		System.out.println(bigSentence.charAt(0));
		System.out.println(bigSentence.charAt(1));
		
		System.out.println("\r\n*** Comparaison entre deux chaînes de caractères");
		// Egalité entre deux chaînes
		System.out.println(sentence1.equals(sentence2));
		// Non égalité entre deux chaînes
		System.out.println(!sentence1.equals(sentence2));
	}
}
