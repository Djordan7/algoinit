package fr.eql.ai110.algo.init.tp;

import java.util.Random;
import java.util.Scanner;

public class Exercice11 {

	public static void main(String[] args) {
		
		int nbMatches;
		int nbMatchesPlayer = 0;
		int nbMatchesComputer = 0;
		String restart = "o";
		String start;
		Random random = new Random();
		Scanner scanner;

		do {
			scanner = new Scanner(System.in);
			nbMatches = 20;
			
			System.out.println("BIENVENUE DANS LE JEU DE NIM !");
			System.out.println("Il y a " + nbMatches + " allumettes.");
			System.out.println("Vous pouvez prendre 1, 2 ou 3 allumettes.");
			System.out.println("Celui qui prend la dernière allumette a gagné.");
			System.out.println("Qui commence ? (joueur/ordinateur)");
			start = scanner.nextLine();
			
			while (nbMatches >= 1) {

				if (start.equals("joueur")) {
					System.out.println("C'est à vous de jouer. Combien prenez vous d'allumettes ?");
					nbMatchesPlayer = scanner.nextInt();
					while(nbMatchesPlayer <=0 || nbMatchesPlayer > 3) {
						System.out.println("Tricheur! On a dit 3 allumettes max ! Recommence !");
						nbMatchesPlayer = scanner.nextInt();
					}

					nbMatches -= nbMatchesPlayer;
					System.out.println("Il reste " + nbMatches + " allumette(s).");

					if (nbMatches == 0) {
						System.out.println("Le joueur 1 a gagné.");

					} 

					nbMatchesComputer = 4 - nbMatchesPlayer;

					nbMatches -= nbMatchesComputer;
					System.out.println("L'ordinateur a pris " + nbMatchesComputer + " allumette(s).");
					System.out.println("Il reste " + nbMatches + " allumette(s).");

					if (nbMatches == 0) {
						System.out.println("L'ordinateur a gagné.");

					} 
				} else {
					switch (nbMatches) {
					case 1:
						nbMatchesComputer = 1;
						break;
					case 2:
						nbMatchesComputer = 2;
						break;
					case 3:
						nbMatchesComputer = 3;
						break;
					default:
						nbMatchesComputer = random.nextInt(3) +1;
						break;
					}
					nbMatches -= nbMatchesComputer;
					System.out.println("L'ordinateur a pris " + nbMatchesComputer + " allumette(s).");
					System.out.println("Il reste " + nbMatches + " allumette(s).");

					if (nbMatches == 0) {
						System.out.println("L'ordinateur a gagné.");

					} 
					if (nbMatches > 0) {
						System.out.println("C'est à vous de jouer. Combien prenez-vous d'allumettes ?");
						nbMatchesPlayer = scanner.nextInt();
						while(nbMatchesPlayer <=0 || nbMatchesPlayer > 3) {
							System.out.println("Tricheur! On a dit 3 allumettes max ! Recommence !");
							nbMatchesPlayer = scanner.nextInt();
						}

						nbMatches -= nbMatchesPlayer;
						System.out.println("Il reste " + nbMatches + " allumette(s).");

						if (nbMatches == 0) {
							System.out.println("Le joueur 1 a gagné.");

						} 
					}					
				}
			}
			System.out.println("Voulez-vous rejouer ? (o ou n)");
			restart = scanner.next();
		} while (restart.equals("o"));
		scanner.close();
		System.out.println("Fin du jeu.");
	}
}

