package fr.eql.ai110.algo.init.tp;

import java.util.Scanner;

public class Exercice3 {

	public static void main(String[] args) {
		int number;
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Veuillez entrer un entier :");
		number = myScanner.nextInt();
		myScanner.close();
		
		if (number % 2 == 0) {
			System.out.println("Résultat si le nombre est pair : "
		    + number / 2);
		} else {
			System.out.println("Résultat si le nombre est impair : "
		    + (3 * number + 1));
		}
		// Opérateur ternaire :
		// condition ? instruction 1 : instruction 2
		// équivalent de 
		// if (condition) {
		//	instruction 1;
		// } else {
		//  instruction 2
		// }
		int result = number % 2 == 0 ? number / 2 : 3 * number + 1;
		System.out.println(result);
	}
}
