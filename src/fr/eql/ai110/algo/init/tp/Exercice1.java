package fr.eql.ai110.algo.init.tp;

import java.util.Scanner;

public class Exercice1 {

	public static void main(String[] args) {
		
		int number;
		Scanner monScanner = new Scanner(System.in);
		System.out.println("Entrez un nombre");
		number = monScanner.nextInt();
		if (number > 0) {
			System.out.println("Votre nombre est positif.");
		} else if (number < 0) {
			System.out.println("Votre nombre est négatif.");
		} else {
			System.out.println("Votre nombre est égal à 0.");
		}
		monScanner.close();
	}
}
