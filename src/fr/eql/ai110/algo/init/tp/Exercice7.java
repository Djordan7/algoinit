package fr.eql.ai110.algo.init.tp;

import java.util.Scanner;

public class Exercice7 {

	public static void main(String[] args) {
		
		int number;
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Veuillez choisir un nombre :");
		number = myScanner.nextInt();
		System.out.println("Table de " + number + " :");
		for (int i = 1; i <= 10; i++) {
			System.out.println(number + " * " + i + " = " + number * i);
		}
		myScanner.close();
	}
}
