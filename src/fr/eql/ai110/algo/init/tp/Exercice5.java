package fr.eql.ai110.algo.init.tp;

import java.util.Scanner;

public class Exercice5 {

	public static void main(String[] args) {
		
		// Déclaration de variables
		Scanner myScanner = new Scanner(System.in);
		int a, b;
		String login;
		String password;
		String operation;
		boolean operationOK = true;
		float result = 0;
		
		System.out.println("Entrez votre login");
		login = myScanner.next();
		System.out.println("Entrez votre password");
		password = myScanner.next();
		
		if (login.equals("admin") && password.equals("123Soleil")) {
			System.out.println("Quelle opération souhaitez-vous effectuer ?"
					+ " (addition/soustraction/multiplication/division)");
			operation = myScanner.next();
			System.out.println("Entrez un premier opérande :");
			a = myScanner.nextInt();
			System.out.println("Entrez un deuxième opérande :");
			b = myScanner.nextInt();
			
			switch (operation) {
			case "addition":
				result = a + b;
				break;
			case "soustraction":
				result = a - b;
				break;
			case "multiplication":
				result = a * b;
				break;
			case "division":
				result = a / b;
				break;
			default:
				System.out.println("L'opération demandée n'existe pas.");
				operationOK = false;
				break;
			}
			if (operationOK) {
				System.out.println("Le résultat de l'opération " + operation +
						" avec les opérandes " + a + " et " + b + " est : " +
						result);
			}
		} else {
			System.out.println("Mauvais login ou password");
		}
		System.out.println("Fin de programme");
		myScanner.close();
	}
}
