package fr.eql.ai110.algo.init.tp;

public class Exercice9 {

	public static void main(String[] args) {
		
		int size = 20;
		int nbSpace, nbStar;
		
		// Pour chaque étage de la pyramide
		for (int row = 0; row < size; row++) {
			// Calculer le nombre d'espaces à afficher
			nbSpace = size - row - 1;
			// Calculer le nombre d'étoiles à afficher
			nbStar = 2 * row + 1;
			// Afficher le nombre d'espace nécessaire
			for (int i = 0; i < nbSpace; i++) {
				System.out.print(" ");
			}
			// Afficher le nombre d'étoiles nécessaire
			for (int i = 0; i < nbStar; i++) {
				System.out.print("*");
			}
			// On passe à la ligne
			System.out.println();
		}
	}
}
