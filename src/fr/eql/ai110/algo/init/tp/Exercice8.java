package fr.eql.ai110.algo.init.tp;

import java.util.Scanner;

public class Exercice8 {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		double result = 1;
		System.out.println("Veuillez choisir un nombre strictement"
				+ " positif :");
		int number = myScanner.nextInt();
		myScanner.close();
		if (number > 0) {
			for (int i = 1; i <= number; i++) {
				result *= i;
			}
		} else {
			System.out.println("Vous n'avez pas choisi un nombre positif.");
		}
		System.out.println("Le résultat du produit des entiers de 1 "
				+ "jusqu'à " + number + " est " + result);
	}
}
