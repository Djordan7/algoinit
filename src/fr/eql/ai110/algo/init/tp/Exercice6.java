package fr.eql.ai110.algo.init.tp;

import java.util.Scanner;

public class Exercice6 {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		int number;
		System.out.println("Veuillez choisir un nombre :");
		number = myScanner.nextInt();
		for (int i = number + 1; i <= number + 10; i++) {
			System.out.print(i + " ");
		}
		myScanner.close();
	}
}
