package fr.eql.ai110.algo.init.tp;

import java.util.Random;
import java.util.Scanner;

public class Exercice10 {

	public static void main(String[] args) {
		
		Random random= new Random();
		Scanner scanner = new Scanner(System.in);
		String replay = "";
		int score = 0;
		
		//Boucle de jeu
		do {
			int target = random.nextInt(50) + 1;
			int playerInput;
			int nbMaxTries = 5;
			int nbTry = 0;
			
			// Boucle de partie
			// Tant que le joueur n'a pas trouve la cible et tant que le nombre d'essais max
			// n'a pas été épuisé
			do {
				nbTry++;
				System.out.println("Entrez un nombre :");
				playerInput = scanner.nextInt();
				scanner.nextLine();
				
				// On donne un indice
				if (playerInput > target) {
					System.out.print("Trop grand ! ");
				} else if (playerInput < target) {
					System.out.print("Trop petit ! ");
				}
				
				// On indique le nombre d'essais restant
				if (nbMaxTries - nbTry > 0 && playerInput != target) {
					System.out.println("Encore " + (nbMaxTries - nbTry) + " essai(s).");
				}
			} while (playerInput != target && nbTry < nbMaxTries);
			
			//Si le joueur a trouvé le bon nombre
			if (playerInput == target) {
				score++;
				System.out.println("Bravo ! Vous avez trouvé en " + nbTry + " essais. Votre score : " + score);
			} else {
				//Si le joueur n'a pas trouvé le bon nombre
				score--;
				System.out.println("Dommage... Le nombre à trouver était " + target + ".\r\n Votre score : " + score);
					
			}
			
			//Sortie de partie 		
			System.out.println("Voulez vous rejouer ? (o/n)");
			replay = scanner.nextLine();
		} while (!replay.equals("n"));
		System.out.println("Fin du jeu.");
		scanner.close();
	}
}
