package fr.eql.ai110.algo.init.tp;

import java.util.Scanner;

public class Exercice2 {

	public static void main(String[] args) {
		
		int number;
		Scanner monScanner = new Scanner(System.in);
		System.out.println("Veuillez entrer un entier :");
		number = monScanner.nextInt();
		if (number % 2 == 0) {
			System.out.println("Votre entier est pair.");
		} else {
			System.out.println("Votre entier est impair.");
		}
		monScanner.close();
	}
}
